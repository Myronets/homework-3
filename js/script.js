// Задание
// Реализовать возможность смены цветовой темы сайта пользователем.
//
//     Технические требования:
//
//     Взять любое готовое домашнее задание по HTML/CSS.
//     Добавить на макете кнопку "Сменить тему".
//     При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.)
//     на ваше усмотрение. При повтором нажатии - возвращать все как было изначально -
//     как будто для страницы доступны две цветовых темы.
//     Выбранная тема должна сохраняться и после перезагрузки страницы

let button = document.createElement('button');
button.innerHTML = 'Change style';
button.classList.add('button-style');
document.body.appendChild(button);

let style = document.getElementById('style');

if (localStorage.getItem('userStyle')) {
    style.href = localStorage.getItem('userStyle')
}

button.addEventListener('click', ()=> {
    changeStyle();
    localStorage.setItem('userStyle', style.href);
});

function changeStyle() {
     if (style.href === 'http://localhost:63342/homework-3/css/style.css' ) {
        style.href = 'http://localhost:63342/homework-3/css/style-green.css'
    } else {style.href = 'http://localhost:63342/homework-3/css/style.css'}
}